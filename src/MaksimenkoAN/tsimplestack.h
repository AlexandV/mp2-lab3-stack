#ifndef __TSIMPLESTACK_H__
#define __TSIMPLESTACK_H__

#include <exception>
#include "C:\Users\Jarvi\Desktop\Labs\Lab5(Stak)\mp3-lab3-stack\include\tdatacom.h"

#define DefMemSize 25 

#define DataEmpty -101 

#define DataFull -102 

template <class TypeData = int, int N = DefMemSize>
class TSimpleStack : public TDataCom
{
private:
	int DataCount;
	int last;
	TypeData Data[N];
public:
	TSimpleStack();// �����������
	void Put(const TypeData &); //������� �������� � ����;
	TypeData Get(void); //������� �������� �� �����; 
	void Print(); //����������� �������� �����;
};
//����������:
template <class TypeData, int N>// �����������
TSimpleStack <TypeData, N> ::TSimpleStack() : last(-1), DataCount(0){}

template <class TypeData, int N> //������� �������� � ����;
void TSimpleStack <TypeData, N> ::Put(const TypeData &Val)
{
	if (DataCount == N)
		SetRetCode(DataFull);
	else
	{
		DataCount++;
		Data[++last] = Val;
	}
}

template <class TypeData, int N> //������� �������� �� �����; 
TypeData TSimpleStack <TypeData, N> ::Get(void)
{
	TData result = -1;
	if (DataCount == 0) SetRetCode(DataEmpty);
	else
	{
		DataCount--;
		result = Data[last--];
	}
}

#endif

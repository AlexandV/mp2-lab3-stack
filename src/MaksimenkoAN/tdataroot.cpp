#include "C:\Users\Jarvi\Desktop\Labs\Lab5(Stak)\mp3-lab3-stack\include\tdataroot.h"
#include <iostream>

TDataRoot::TDataRoot(int Size) //�����������
{
	DataCount = 0;
	MemSize = Size;
	if (Size == 0)
	{
		MemType = MEM_RENTER;
		pMem = NULL;
	}
	else
	{
		MemType = MEM_HOLDER;
		pMem = new TElem[Size];
	}
}

TDataRoot :: ~TDataRoot() 
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
}

void TDataRoot::SetMem(void *pMem, int Size) // ������� ������
{
	pMem = (TElem *)pMem;
	MemSize = Size;
}

bool TDataRoot::IsEmpty() const // �������� ������� ��
{
	return DataCount == 0;
}

bool TDataRoot::IsFull() const // �������� ������������ ��
{
	return DataCount == MemSize;
}

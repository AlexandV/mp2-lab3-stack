#include "C:\Users\Jarvi\Desktop\Labs\Lab5(Stak)\mp3-lab3-stack\src\tstack.h"
#include "gtest.h"

TEST(TStack, create_the_stack_with_a_positive_length) //�������� ����� ������������� �����
{
	ASSERT_NO_THROW(TStack st(7));
}

TEST(TStack, create_the_stack_with_a_negative_length) //�������� ����� ������������� �����
{
	ASSERT_ANY_THROW(TStack st(-7));
}

TEST(TStack, fill_the_stack_to_the_maximum) //���������� ����� �� ���������
{
	const int size = 2;
	TStack S (size);
	S.Put(6);
	S.Put(7);
	EXPECT_EQ(size, S.GetCount());
}

TEST(TStack, cleanup_stack) //������� �����
{
	const int size = 2;
	TStack S(size);
	S.Get();
	S.Get();
	EXPECT_EQ(0, S.GetCount());
}

TEST(TStack, well_gets_value) // ��������� ��������
{
	const int size = 2;
	TStack S(size);
	S.Put(5);
	S.Put(7);
	EXPECT_EQ(5, S.Get());
	EXPECT_EQ(7, S.Get());
}

TEST(TStack, stack_walkon_the_void_filleng) // �������� ������� ��� ���������� ��������
{
	TStack S(0);
	S.Put(1);
	EXPECT_EQ(DataNoMem, S.GetRetCode());
}

TEST(TStack, stack_walkon_the_void_filleng_clean) // �������� ������� ��� ���������� ��������
{
	TStack S(0);
	S.Get();
	EXPECT_EQ(DataNoMem, S.GetRetCode());
}

TEST(TStack, can_get_code_return_is_full) //��������� ���� ��������
{
	const int size = 2;
	TStack S(size);
	S.Put(1);
	S.Put(1);
	S.Put(1);
	EXPECT_EQ(DataFull, S.GetRetCode());
}

TEST(TStack, fill_the_stack_in_accordance_with_the_size)// ����������,�� ����������� ������,�����
{
	const int size = 7;
	TStack S(size);
	S.Put(1);
	S.Put(1);
	S.Put(1);
	S.Put(1);
	S.Put(1);
	S.Put(1);
	S.Put(1);
	EXPECT_NE(size, S.GetCount());
}

TEST(TStack, fill_the_stack_in_accordance_with_the_size)// ����������,�� ����������� ������,����� � ��� �������
{
	const int size = 7;
	TStack S(size);
	for (int i = 0; i < size;i++)
		S.Put(1);

	for (int i = 0; i < size; i++)
		S.Get();

	EXPECT_NE(0, S.GetCount());
}

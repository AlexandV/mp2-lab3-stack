#include "C:\Users\Jarvi\Desktop\Labs\Lab5(Stak)\mp3-lab3-stack\src\tstack.h"
#include <iostream>

TStack::TStack(int Size) : last(-1), TDataRoot(Size){}

void TStack::Put(const TData &Val) //������� �������� � ����;
{
	if (pMem == NULL) SetRetCode(DataNoMem);
	else 
		if (IsFull()) SetRetCode(DataFull);
		else
		{
			DataCount++;
			pMem[++last] = Val;
		}
}

TData TStack::Get(void) //������� �������� �� �����; 
{
	TData result = -1;
	if (pMem == NULL) SetRetCode(DataNoMem);
	else 
		if (IsEmpty()) SetRetCode(DataEmpty);
		else
		{
			DataCount--;
			result = pMem[last--];
		}
	return result;
}

int TStack::GetCount() //���-�� ��������� � �����;
{
	return DataCount;
}

void TStack::Print() //����������� �������� �����;
{
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << ' ';
	cout << endl;
}
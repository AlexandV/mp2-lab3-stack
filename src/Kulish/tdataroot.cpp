#include "tdataroot.h"
#include <iostream>

using namespace std;

TDataRoot::TDataRoot(int Size) : MemSize(Size), pMem((Size == 0) ? NULL : new TElem[Size]), DataCount(0)
{
	MemType = (Size == 0) ? MEM_RENTER : MEM_HOLDER;
}
//���� ������ ����� ����� �������, ����� ������� ����� ��������� SetMem
//���� �� ������� �������� ������ ����� ���������� ����� ��������

TDataRoot:: ~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
}

void TDataRoot::SetMem(void *p, int Size)
{
	pMem = (TElem *)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty() const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull() const
{
	return DataCount == MemSize;
}
#pragma once
template <class ValType,int T>

class SimpleStack
{
private:
	unsigned int stacksize;
	ValType stack[T];
public:
	SimpleStack();
	void SetEl(ValType);
	ValType GetEl();
};

template <class ValType,int T>
SimpleStack<ValType,  T>::SimpleStack()
{
	stacksize = -1;
}

template <class ValType, int T>
void SimpleStack<ValType,  T>::SetEl(ValType el)
{
	stack[stacksize++] = el;
}

template <class ValType, int T>
ValType SimpleStack<ValType,  T>::GetEl()
{
	ValType tmp;
	tmp = stack[stacksize];
	stacksize--;
	return tmp;
}


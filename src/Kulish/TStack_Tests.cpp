#include "TStack.h"
#include "gtest.h"

TEST(TStack, fill_up_to_a_maximum)
{
	TStack A(2);
	A.Put(1);
	A.Put(1);
	EXPECT_EQ(2, A.GetCount());
}

TEST(TStack, filled_stack_compared_with_the_size)
{
	TStack A(3);
	A.Put(1);
	EXPECT_NE(3, A.GetCount());
}

TEST(TStack, fill_up_to_a_maximum_and_removal_to_zero)
{
	TStack A(2);
	A.Put(1);
	A.Put(1);
	A.Get();
	A.Get();
	EXPECT_EQ(0, A.GetCount());
}

TEST(TStack, fill_stack_and_removal_compared_with_the_zero)
{
	TStack A(2);
	A.Put(1);
	A.Put(1);
	A.Get();
	EXPECT_NE(0, A.GetCount());
}


TEST(TStack, memory_is_empty_when_adding)
{
	TStack A(0);
	A.Put(1);
	EXPECT_EQ(DataNoMem, A.GetRetCode());
}

TEST(TStack, memory_is_empty_when_removing)
{
	TStack A(0);
	A.Get();
	EXPECT_EQ(DataNoMem, A.GetRetCode());
}

TEST(TStack, all_is_well)
{
	TStack A(2);
	A.Put(1);
	A.Put(1);
	EXPECT_EQ(DataOK, A.GetRetCode());
}

TEST(TStack, well_gets_value)
{
	TStack A(2);
	A.Put(1);
	A.Put(2);
	EXPECT_EQ(2, A.Get());
	EXPECT_EQ(1, A.Get());
}


TEST(TStack, can_get_code_return_is_empty)
{
	TStack A(2);
	A.Get();
	EXPECT_EQ(DataEmpty, A.GetRetCode());
}
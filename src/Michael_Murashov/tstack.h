#ifndef _TSTACK_
#define _TSTACK_

#include "tdataroot.h"

using namespace std;

class tstack: public TDataRoot
{
protected:
	int top; // ��������� �� ��������� �������

public:
	tstack(int size = 10) : TDataRoot(size) { top = -1; }
	virtual void  Put(const TData &Val); // �������� ������� � ����
	virtual TData Get(); // �������� ��������� �������

	virtual void Print(); // ������ ����� �� �����
	int GetCount(); // �������� ��������� ��������� � �����
};

void tstack::Put(const TData &Val)
{
	try
	{
		if (pMem == NULL)
		{
			SetRetCode(DataNoMem);
			throw exception("DataNoMem");
		}

		if (IsFull())
		{
			SetRetCode(DataFull);
			throw exception("DataFull");
		}

		pMem[++top] = Val;
		DataCount++;
	}

	catch (exception ex)
	{
		cout << ex.what();
	}
}/*--------------------------------------------------------------------*/

TData tstack::Get()
{
	try
	{
		if (pMem == NULL)
		{
			SetRetCode(DataNoMem);
			throw exception("DataNoMem");
		}

		if (IsEmpty())
		{
			SetRetCode(DataEmpty);
			throw exception("DataEmpty");
		}

		TData tmp = pMem[top--];
		DataCount--;

		return tmp;
	}

	catch (exception ex)
	{
		cout << ex.what();
	}
}/*--------------------------------------------------------------------*/

int tstack::GetCount()
{
	return DataCount;
}/*--------------------------------------------------------------------*/

void tstack::Print()
{
	if (DataCount == 0) cout << "Stack is empty!";
	else
		if (pMem == NULL) cout << "Pointer is NULL!";
		else
			for (int i = 0; i < DataCount; i++)
				cout << pMem << ' ';

	cout << endl;
}/*--------------------------------------------------------------------*/

#endif
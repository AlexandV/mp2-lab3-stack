// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include <iostream>
#include <exception>
#include "tdatacom.h"

using namespace std;

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

typedef int    TElem;    // тип элемента СД
typedef TElem* PTElem;
typedef int    TData;    // тип значений в СД

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot: public TDataCom
{
protected:
  PTElem pMem;      // память для СД
  int MemSize;      // размер памяти для СД
  int DataCount;    // количество элементов в СД
  TMemType MemType; // режим управления памятью

  void SetMem(void *p, int Size);             // задание памяти
public:
  TDataRoot(int Size = DefMemSize); 
  virtual ~TDataRoot();
  virtual bool IsEmpty(void) const;           // контроль пустоты СД
  virtual bool IsFull (void) const;           // контроль переполнения СД
  virtual void  Put   (const TData &Val) = 0; // добавить значение
  virtual TData Get   (void)             = 0; // извлечь значение

  // служебные методы
  virtual void Print()   = 0;                 // печать значений

  // дружественные классы
  friend class TMultiStack;
  friend class TSuperMultiStack;
  friend class TComplexMultiStack;
};

TDataRoot::TDataRoot(int Size): TDataCom()
{
	try
	{
		if (Size < 0) throw exception("mem error");
		if (Size == 0) // память выделяется с помощью SetMem дочерними классами
		{
			pMem = NULL;
			MemType = MEM_RENTER;
		}
		else // память выделяется этим классом
		{
			MemType = MEM_HOLDER;
			MemSize = Size;
			DataCount = 0;

			// считаем, сколько элементов хранить в массиве
			int ElemCount = MemSize / sizeof(TElem);
			if (ElemCount == 0)
				pMem = new TElem[1];
			else
				if (MemSize % sizeof(TElem))
					pMem = new TElem[ElemCount];
				else
					pMem = new TElem[ElemCount + 1];
		}
	}

	catch (exception ex)
	{
		cout << ex.what();
	}
}/*--------------------------------------------------------------------*/

TDataRoot::~TDataRoot()
{
	if (MemType == MEM_HOLDER) delete[] pMem;
	// если MemType == MEM_RENTER, память удаляется в дочерних классах
}/*--------------------------------------------------------------------*/

bool TDataRoot::IsEmpty(void) const
{
	if (DataCount == 0)
		return true;

	return false;
}/*--------------------------------------------------------------------*/

bool TDataRoot::IsFull(void) const
{
	if ( (DataCount * sizeof(TElem)) > MemSize )
		return true;

	return false;
}/*--------------------------------------------------------------------*/

void TDataRoot::SetMem(void *p, int Size)
{ }

#endif

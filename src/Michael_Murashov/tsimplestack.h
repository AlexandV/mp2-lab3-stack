#pragma once

#include <iostream>
#include <exception>

using namespace std;

const int Size = 255; //������ �����

typedef char Data;

class tsimplestack
{
private:
	Data pMem[Size]; //������ ��� ��������
	int _top; //��������� �� ��������� �������

public:
	tsimplestack() : _top(-1) {}
	bool empty() const;
	bool full() const;
	void push(Data val);
	Data top();
	void pop() { _top--; }

	int size() { return Size; }
	void print();
};

bool tsimplestack::empty() const
{
	if (_top == -1)
		return true;

	return false;
}/*-----------------------------------------------------------*/

bool tsimplestack::full() const
{
	if (_top >= Size - 1)
		return true;

	return false;
}/*-----------------------------------------------------------*/

void tsimplestack::push(Data val)
{
	try
	{
		if (full()) throw exception("Stack is full!");

		pMem[++_top] = val;
	}

	catch (exception ex)
	{
		cout << ex.what() << endl;
	}
}/*-----------------------------------------------------------*/

Data tsimplestack::top()
{
	try
	{
		if (empty()) throw exception("stack is empty");

		return pMem[_top];
	}

	catch (exception ex)
	{
		cout << ex.what() << endl;
	}
}/*-----------------------------------------------------------*/

void tsimplestack::print()
{
	if (_top == -1) cout << "Stack is empty!" << endl;
	else
		for (int i = 0; i < _top; i++)
			cout << pMem[i] << ' ';

	cout << endl;
}/*-----------------------------------------------------------*/
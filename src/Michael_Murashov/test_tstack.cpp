#include <gtest.h>
#include "tstack.h"

TEST(tstack, can_create_stack_with_positive_length)
{
	ASSERT_NO_THROW(tstack st(2));
}

TEST(tstack, cant_create_stack_with_negative_length)
{
	ASSERT_ANY_THROW(tstack st(-2));
}

TEST(tstack, can_put_in_stack)
{
	tstack st(20);
	ASSERT_NO_THROW(st.Put(2));
}

TEST(tstack, can_get_from_stack)
{
	tstack st(20);
	st.Put(2);
	EXPECT_EQ(2, st.Get());
}

TEST(tstack, cant_put_in_full_stack)
{
	tstack st(8); // ���� �������� 4 ����� - 2 ����
	st.Put(1); // �������� ������ ���
	st.Put(2); // �������� ������ ���
	ASSERT_ANY_THROW(st.Put(2));
}

TEST(tstack, cant_get_from_empry_stack)
{
	tstack st(4); // ������� ������ ����
	ASSERT_ANY_THROW(st.Get());
}

TEST(tstack, can_get_count)
{
	tstack st(8); // ���� �������� 8 ���� - 2 ����
	st.Put(3); // �������� ���� ���
	EXPECT_EQ(1, st.GetCount());
}

TEST(tstack, can_clean_stack)
{
	tstack st(20);
	st.Put(1);
	st.Get();
	EXPECT_EQ(0, st.GetCount());
}

TEST(tstack, can_get_code_1)
{
	tstack st(8);
	st.Put(1);
	st.Put(2);
	EXPECT_EQ(DataFull, A.GetRetCode());
}

TEST(tstack, can_get_code_2)
{
	tstack st(20);
	st.Get();
	EXPECT_EQ(DataEmpty, st.GetRetCode());
}
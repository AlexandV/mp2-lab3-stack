#pragma once

#include "tdataroot.h"

class TSimpleStack : public TDataRoot {
public:
	TSimpleStack(int size = DefMemSize) : TDataRoot(size) {
		MemType = MEM_HOLDER;
	}
	~TSimpleStack();
	bool IsEmpty() const;
	bool IsFull() const;
	void Put(const TData& val);
	TData Get();
	int IsValid();
	void Print();
};
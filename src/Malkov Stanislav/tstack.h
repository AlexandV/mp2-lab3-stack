#pragma once

#include "tdataroot.h"

class TStack : public TDataRoot {
public:
	TStack(int size = DefMemSize) : TDataRoot(size) {
		MemType = MEM_RENTER;
	}
	~TStack();
	bool IsEmpty() const;
	bool IsFull() const;
	void Put(const TData& val);
	TData Get();
	int IsValid();
	void Print();
};
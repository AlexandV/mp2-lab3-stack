#include "tdataroot.h"

TDataRoot::~TDataRoot()
{
	delete[] pMem;
}

TDataRoot::TDataRoot(int Size)
{
	MemSize = Size;
	pMem = new TElem[MemSize];
	DataCount = 0;

	if (Size == 0)
		MemType = MEM_RENTER;
	else
		MemType = MEM_HOLDER;
}

bool TDataRoot::IsEmpty(void) const
{
	return Datacount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}

void TDataRoot::SetMem(void *p, int Size)
{
	pMem = (TElem *)p;
	MemSize = Size;
}
template<class ValType = int, int NUM = 25> 
class TSimpleStack
{
protected:
	ValType Data[NUM];
	int Count;
public:
	TSimpleStack();
	void PutElem(ValType);
	ValType  GetElem();
	int GetCount();
};

template <class ValType, int NUM>
TSimpleStack<ValType, NUM>::TSimpleStack()
{
	Count = 0;
}

template<class ValType, int NUM>
void TSimpleStack<ValType, NUM>::PutElem(ValType n)
{
	if (Count < NUM)
	{
		Data[Count++] = n;
	}
}

template<class ValType, int NUM>
ValType TSimpleStack<ValType, NUM>::GetElem()
{
	if (Count > 0)
	{
		return Data[Count--];
	}
}

template<class ValType, int NUM>
int TSimpleStack<ValType, NUM>::GetCount()
{
	return Count;
}
#include "tdataroot.h"

class TStack : public TDataRoot
{
public:
	TStack(int = DefMemSize);
	virtual void Put(const TData &Val);
	virtual TData Get(void);
	virtual void Print();
	int GetCount();
protected:
	int LastElem;
};
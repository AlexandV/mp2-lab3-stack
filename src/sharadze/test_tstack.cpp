#include "TStack.h"
#include "gtest.h"

TEST(TStack, can_fill_up_to_max)
{
	TStack A (3);
	A.Put(1);
	A.Put(2);
	A.Put(3);
	EXPECT_EQ(3, A.GetCount());
}

TEST(TStack, stack_compares_with_the_size)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	EXPECT_NE(size, A.GetCount());
}

TEST(TStack, fill_up_to_a_maximum_and_remove_to_zero)
{
	TStack A(2);
	A.Put(1);
	A.Put(2);
	A.Get();
	A.Get();
	EXPECT_EQ(0, A.GetCount());
}

TEST(TStack, fill_stack_and_remove_compares_with_null)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	A.Get();
	EXPECT_NE(0, A.GetCount());
}


TEST(TStack, memory_is_empty_when_add)
{
	TStack A(0);
	A.Put(1);
	EXPECT_EQ(DataNoMem, A.GetRetCode());
}

TEST(TStack, memory_is_empty_when_remove)
{
	TStack A(0);
	A.Get();
	EXPECT_EQ(DataNoMem, A.GetRetCode());
}

TEST(TStack, compare_ret_code)
{
	TStack A(2);
	A.Put(1);
	A.Put(2);
	EXPECT_EQ(DataOK, A.GetRetCode() );
}

TEST(TStack, well_gets_value)
{
	TStack A(2);
	A.Put(1);
	A.Put(2);
	EXPECT_EQ(2, A.Get() );
	EXPECT_EQ(1, A.Get() );
}

TEST(TStack, can_get_ret_code_when_is_full)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	A.Put(1);
	EXPECT_EQ(DataFull, A.GetRetCode() );
}

TEST(TStack, can_get_ret_code_is_empty)
{
	TStack A(3);
	A.Get();
	EXPECT_EQ(DataEmpty, A.GetRetCode() );
}

TEST(TStack, can_get_right_ret_code_after_getting)
{
	TStack A(2);
	A.Put(1);
	A.Put(2);
	A.Put(3);
	A.GetRetCode();
	EXPECT_EQ(DataOK, A.GetRetCode() );
}
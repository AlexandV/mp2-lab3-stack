#include "TStack.h"
#include <iostream>

using namespace std;

TStack::TStack(int Size)
{
	TDataRoot(Size);
	LastElem = -1;
}

void TStack::Put(const TData &Val)
{
	if (pMem == NULL) 
		SetRetCode(DataNoMem);
	else
		if (IsFull()) 
			SetRetCode(DataFull);
		else
		{
			pMem[++LastElem] = Val;
			DataCount++;
		}
}

TData TStack::Get(void)
{
	if (pMem == NULL) 
		SetRetCode(DataNoMem);
	else
		if (IsEmpty()) 
			SetRetCode(DataEmpty);
		else
		{
			DataCount--;
			return pMem[LastElem--];
		}
}

void TStack::Print()
{
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << ' ';
	cout << endl;
}

int TStack::GetCount()
{
	return DataCount;
}
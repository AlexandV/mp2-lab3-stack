#include "tstack.h"
#include "tsimplestack.h"
#include <gtest.h>

// tstack

TEST(can_get_datanomem_code_when_put)
{
	TStack st(0);
	st.Put(1);
	EXPECT_EQ(DataNoMem, st.GetRetCode());
}

TEST(can_get_datanomem_code_when_get)
{
	TStack st(0);
	st.Get();
	EXPECT_EQ(DataNoMem, st.GetRetCode());
}

TEST(can_get_dataempty_code)
{
	TStack st(1);
	st.Get();
	EXPECT_EQ(DataEmpty, st.GetRetCode());
}

TEST(can_get_datafull_code)
{
	TStack st(2);
	st.Put(1);
	st.Put(2);
	st.Put(3);
	EXPECT_EQ(DataFull, st.GetRetCode());
}

TEST(can_get_dataok_code)
{
	TStack st(1);
	st.Put(1);
	EXPECT_EQ(DataOK, st.GetRetCode());
}

TEST(obtained_values_are_equal_given)
{
	TStack st(2);
	st.Put(1);
	st.Put(2);
	EXPECT_EQ(2, st.Get());
	EXPECT_EQ(1, st.Get());
}

TEST(full_stack_equal_size)
{
	int Size = 2;
	TStack st(Size);
	st.Put(1);
	st.Put(2);
	EXPECT_EQ(Size, st.GetDataCount());
}

TEST(not_full_stack_not_equal_size)
{
	int Size = 3;
	TStack st(Size);
	st.Put(1);
	st.Put(2);
	EXPECT_NE(Size, st.GetDataCount());
}

TEST(filling_and_cleaning_stack_to_zero)
{
	int Size = 2;
	TStack st(Size);
	st.Put(1);
	st.Put(2);
	st.Get();
	st.Get();
	EXPECT_EQ(0, st.GetDataCount());
}

TEST(filling_and_cleaning_stack_not_to_zero)
{
	int Size = 3;
	TStack st(Size);
	st.Put(1);
	st.Put(2);
	st.Put(3);
	st.Get();
	st.Get();
	EXPECT_NE(0, st.GetDataCount());
}

//simple stack

TEST(full_simplestack_equal_size)
{
	TSimpleStack st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);
	EXPECT_EQ(MemSize, st.GetTop() + 1);
}

TEST(not_full_simplestack_not_equal_size)
{
	TSimpleStack st;
	st.Put(1);
	st.Put(2);
	EXPECT_NE(MemSize, st.GetTop() + 1);
}

TEST(filling_and_cleaning_simplestack_not_to_zero)
{
	TSimpleStack st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);
	for (int i = 0; i < MemSize - 1; i++)
		st.Get();
	EXPECT_NE(0, st.GetTop() + 1);
}

TEST(filling_and_cleaning_simplestack_to_zero)
{
	TSimpleStack st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);
	for (int i = 0; i < MemSize; i++)
		st.Get();
	EXPECT_EQ(0, st.GetTop() + 1);
}

TEST(simple_obtained_values_are_equal_given)
{
	TSimpleStack st;
	st.Put(1);
	st.Put(2);
	EXPECT_EQ(2, st.Get());
	EXPECT_EQ(1, st.Get());
}

TEST(simple_throw_when_stack_is_full)
{
	TSimpleStack st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);
	ASSERT_ANY_THROW(st.Put(1));
}

TEST(simple_throw_when_stack_is_empty)
{
	TSimpleStack st;
	ASSERT_ANY_THROW(st.Get());
}
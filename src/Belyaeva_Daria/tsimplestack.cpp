#ifndef __TSIMPLESTACK_H__
#define __TSIMPLESTACK_H__

#include <iostream>

#define MemSize   25  // размер памяти по умолчанию
typedef int TData;  

class TSimpleStack {
protected:
	TData pMem[MemSize]; // память
	int Top; // индекс последнего элемента
public:
	TSimpleStack(): Top(-1) {}
	int GetTop(void) {
		return Top;
	}
	bool IsEmpty(void) const {
		return (Top == -1);
	} 
	bool IsFull(void) const {
		return (Top == MemSize - 1);
	} 
	void Put(const TData &Val) // добавить элемент
	{ 
		try 
		{
			if (IsFull())
				throw 1;
			pMem[++Top] = Val;
		}
		catch (int error)
		{
			if (error == 1)
				std::cout << "Stack is full" << std::endl;
			throw 1;
		}
	} 
	TData Get(void) //извлечь элемент
	{ 
		try 
		{
			if (IsEmpty())
				throw 1;
			return pMem[Top--];
		}
		catch (int error)
		{
			if (error == 1)
				std::cout << "Stack is empty" << std::endl;
			throw 1;
		}
	} 
};

#endif
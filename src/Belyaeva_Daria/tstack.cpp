#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"
#include <stdio.h>
#include <iostream>

class TStack: public TDataRoot {
protected:
	int Top; // индекс последнего элемента структуры
public:
	TStack(int Size = DefMemSize) : Top(-1), TDataRoot(Size) {}
	virtual void Put(const TData &Val);
	virtual TData Get(void);
	virtual void Print();
	int GetDataCount() { return DataCount; }
};

void TStack::Put(const TData &Val)
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsFull())
		SetRetCode(DataFull);
	else
	{
		pMem[++Top] = Val;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	TData result = -1;
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else
	{
		result = pMem[Top--];
		DataCount--;
	}
	return result;
}

void TStack::Print()
{
	for (int i = 0; i < DataCount; i++)
		std::cout << pMem[i] << ' ';
	std::cout << std::endl;
}

#endif
#ifndef _TSTACK_H
#define _TSTACK_H

#include "tdataroot.h"
#include <exception>

using namespace std;

class TStack : public TDataRoot
{
protected:
	int Top; //������ ���������� ��������
public:
	TStack(int = DefMemSize);
	virtual void  Put(const TData &Val); // �������� ��������
	virtual TData Get(void); // ������� ��������
	virtual void Print(); // ������ ��������
	int GetCount();//��������� ���������� ��������� � �����
	TData HGet(void)
	{
		return pMem[Top];
	}
};

#endif

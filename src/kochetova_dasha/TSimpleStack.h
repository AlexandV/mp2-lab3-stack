#ifndef __TSIMPLESTACK_H__
#define __TSIMPLESTACK_H__

#include <iostream>

#define MaxSize 25  // ������ ������ �� ���������

typedef int TData; // ��� �������� � ��

class TSimpleStack {
protected:
	TData pMem[MaxSize]; // ������ ��� ��
	int Last;           // ������ ���������� ��������, �������� � ������
public:
	TSimpleStack(): Last(-1) {}
	int GetLast(void) { return Last; }
	bool IsEmpty(void) const { return (Last == -1); }       // �������� �������
	bool IsFull(void) const { return (Last == MaxSize-1); } // �������� ������������
	void Put(const TData &Val)          // �������� ��������
	{ 
		try 
		{
			if (IsFull()) throw 1;
			pMem[++Last]=Val;
		}
		catch (int error)
		{
			if (error == 1) 
				std::cout << "Stack is full" << std::endl; 
			    throw 1;
		}
	} 
	TData Get(void) //������� �������� 
	{ 
		try 
		{
			if (IsEmpty()) throw 1;
			return pMem[Last--];
		}
		catch (int error)
		{
			if (error == 1)
				std::cout << "Stack is empty" << std::endl; 
			    throw 1;
		}
	} 
};

#endif
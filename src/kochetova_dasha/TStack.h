#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <stdio.h>
#include <iostream>
#include "tdataroot.h"

class TStack: public TDataRoot 
{
protected:
	int Last; // ������ ���������� ��������
public:
	TStack(int Size) : Last(-1), TDataRoot(Size) {}
	int GetDataCount() { return DataCount; }

	virtual void Put(const TData &Val);
	virtual TData Get(void);
	virtual void Print();
};

void TStack::Put(const TData &Val)
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsFull())
		SetRetCode(DataFull);
	else
	{
		pMem[++Last]=Val;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	TData result = -1;
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else
	{
		result = pMem[Last--];
		DataCount--;
	}
	return result;
}

void TStack::Print()
{
	for (int i=0; i<DataCount; i++)
		std::cout << pMem[i] << ' ';
	std::cout << std::endl;
}

#endif


